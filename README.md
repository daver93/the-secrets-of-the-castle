# The Secrets of The Castle #

Genre: 1st Person Horror / Mystery Game

** This game is still under development **


## Story ##

It’s almost dark outside. The player of the game is a man who had a problem with his car just some meters away from a castle. In order to seek help, he is walking in a path that he didn’t know that it exists. Unfortunately, a landslide closed the way back to his car. And suddenly, he found a castle. He must go inside to ask for help.
Inside, the castle seems to have stayed in the condition that was a long time ago, during the medieval years. It is so dark and there are just a few lights spared in the place. Additionally, it seems that there is no one inside.
But, wait. The door to go out is locked now. Our player does not know who (or what) locked it. Now, he has to find another exit. And that means that he has to go deeper in this mysterious and frightened castle.
In the end of this room there is a stair. He goes up and a big spider suddenly dropped in front of him. He walks more and he finds a table. This table has some candles, some dinnerware and flashlight. He picks up the flashlight and now he can have some light to see better the place.
He goes back and he enters in the door in the left. There are some strange things there. At first, he notices a table with chemistry things. Also, some wardrobes and behind in one of them there is a handler. He pulls the lever and he searches everything in the drawers. He finds a key. He keeps it just in case. When he leaves from the room he notices that there was a secret door near to the table. Now, this door is open. He must go check this room, but there is a scary zombie. If the zombie catches him, he will be dead.
After, that he must searches more rooms, until the moment that he finds an old prison. He must go down and open the door with the key that he found in the secret place.
When the door opens, it’s the end of this game..for now.
