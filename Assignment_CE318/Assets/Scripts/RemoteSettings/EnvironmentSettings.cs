﻿using UnityEngine;
using Unity.RemoteConfig;
using UnityEngine.Rendering;
using System;

public class EnvironmentSettings : MonoBehaviour
{

    private Color _ambientSkyColor;
    private AmbientMode _m_AmbientMode;

    public struct userAttributes { }
    public struct appAttributes { }

    void Awake()
    {

        // Add a listener to apply settings when successfully retrieved:
        ConfigManager.FetchCompleted += ApplyRemoteSettings;
        Debug.Log("Awake.");
        // Set the user’s unique ID:
        //ConfigManager.SetCustomUserID("some-user-id");

        // Set the environment ID:
        ConfigManager.SetEnvironmentID("380444c3-1b04-4bd0-97a5-c979b97dccb4");
        

        // Fetch configuration setting from the remote service:
        ConfigManager.FetchConfigs<userAttributes, appAttributes>(new userAttributes(), new appAttributes());

        

        //Debug.Log(ConfigManager.appConfig.config);
        //Debug.Log(ConfigManager.appConfig.assignmentID);
        //Debug.Log(ConfigManager.appConfig.environmentID);


        //RenderSettings.ambientSkyColor
    }

    void ApplyRemoteSettings(ConfigResponse configResponse)
    {
        Debug.Log("ApplyRemoteSettings.");
        // Conditionally update settings, depending on the response's origin:
        switch (configResponse.requestOrigin)
        {
            case ConfigOrigin.Default:
                Debug.Log("No settings loaded this session; using default values.");
                break;
            case ConfigOrigin.Cached:
                Debug.Log("No settings loaded this session; using cached values from a previous session.");
                break;
            case ConfigOrigin.Remote:
                Debug.Log("New settings loaded this session; update values accordingly.");

                 _ambientSkyColor = JsonUtility.FromJson<Color>(ConfigManager.appConfig.GetJson("m_AmbientSkyColor"));
                Debug.Log("_ambientSkyColor: " + _ambientSkyColor);
                RenderSettings.ambientSkyColor = _ambientSkyColor;

                _m_AmbientMode = GetAmbientMode(Int32.Parse(ConfigManager.appConfig.GetString("m_AmbientMode")));
                Debug.Log("_m_AmbientMode: " + _m_AmbientMode);
                RenderSettings.ambientMode = _m_AmbientMode;
                break;
        }
    }

    private AmbientMode GetAmbientMode(int modeNumber)
    {
        switch (modeNumber)
        {
            case 0:
                return AmbientMode.Skybox;
            case 1:
                return AmbientMode.Trilight;
            case 3:
                return AmbientMode.Flat;
            case 4:
                return AmbientMode.Custom;
            default:
                return AmbientMode.Flat;
        }
    }
}
