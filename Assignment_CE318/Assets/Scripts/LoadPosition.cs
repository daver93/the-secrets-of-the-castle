﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class LoadPosition : MonoBehaviour
{

    public FirstPersonController firstPersonController;

    private void Start()
    {
        if (GameMaster.Instance.isSaved)
        {
            firstPersonController = GetComponent<FirstPersonController>();
            firstPersonController.enabled = false;

            this.gameObject.transform.position = new Vector3(GameMaster.Instance.playerX, GameMaster.Instance.playerY, GameMaster.Instance.playerZ);

            StartCoroutine(EnablePlayerScriptCoroutine());
        }
    }

    private void enablePlayerScript()
    {
        firstPersonController.enabled = true;
    }


    IEnumerator EnablePlayerScriptCoroutine()
    {
        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(0.1f);

        //After it waited 0.1 seconds print the time again.
        Debug.Log("Finished Coroutine at timestamp : " + Time.time);

        enablePlayerScript();
    }

}
