﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{

    public static GameMaster Instance;

    public float musicLevel;
    public bool playMusic;
    public int difficulty;

    public bool isWinner;

    public bool isSaved;

    public bool hasTheSilverKey;
    public bool hasTheGoldenKey;
    public bool hasTheFlashlight;

    public float playerX;
    public float playerY;
    public float playerZ;

    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

}
