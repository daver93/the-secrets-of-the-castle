﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckWinning : MonoBehaviour
{

    public bool isWinner;

    public Animator FadeInImage;

    private void Start()
    {
        isWinner = false;
        GameMaster.Instance.isWinner = isWinner;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "WinningPoint")
        {
            isWinner = true;
            GameMaster.Instance.isWinner = isWinner;
            Debug.Log("Must Fade");
            FadeInImage.SetTrigger("fade");
        }
    }
}
