﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LifeHandler : MonoBehaviour
{
    public bool killed;

    private void Start()
    {
        killed = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            killed = true;
            SceneManager.LoadScene("EndMenu");
        }
    }
}
