﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueHandlers : MonoBehaviour
{
    public Text dialogueText;

    public enum Messages {};

    private void Start()
    {
        dialogueText.text = "";
    }

    public void SetDialogueText(string dialogue)
    {
        dialogueText.text = dialogue;
    }
}
