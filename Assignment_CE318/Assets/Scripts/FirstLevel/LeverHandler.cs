﻿using UnityEngine;

public class LeverHandler : MonoBehaviour
{
    public bool isUp;

    public Animator animator;
    public GameController gameController;

    void Start()
    {
        isUp =  true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            PlayerIsNearToTheHandler();
        }
    }

    private void PlayerIsNearToTheHandler()
    {
        if (isUp)
        {
            gameController.LeverText(true);
            if (Input.GetButtonDown("Collect"))
            {
                UseLever();
            }
        }
        else
        {
            gameController.LeverText(false);
        }
    }

    public void UseLever()
    {
        isUp = false;
        animator.SetBool("isUp", false);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            gameController.LeverText(false);
        }
    }
}
