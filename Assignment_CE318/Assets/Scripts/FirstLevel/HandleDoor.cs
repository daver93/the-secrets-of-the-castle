﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleDoor : MonoBehaviour
{

    public bool isOpen;

    public Animator handleDoor;

    public GameController gameController;

    public bool playerCollisionWithDoor;

    public bool isLock;

    void Start()
    {
        isOpen = false;
        handleDoor.SetBool("isOpen", isOpen);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerCollisionWithDoor = true;
            gameController.doorHandlingText.gameObject.SetActive(true);

            if (!isLock)
            {
                if (gameController.HandleDoor())
                {
                    if (handleDoor.GetBool("isOpen"))
                    {
                        //Debug.Log("The door must open now");
                        handleDoor.SetBool("isOpen", false);
                        this.isOpen = false;
                    }
                    else
                    {
                        //Debug.Log("The door must close now");
                        handleDoor.SetBool("isOpen", true);
                        this.isOpen = true;
                    }
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        playerCollisionWithDoor = false;
    }

    public bool GetLock()
    {
        return this.isLock;
    }

    public void SetLock(bool setLock)
    {
        isLock = setLock;
    }
}
