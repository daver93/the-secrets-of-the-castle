﻿using UnityEngine;

public class TakeFlashlight : MonoBehaviour
{
    private GameObject _player;

    public bool pickup;

    public GameObject flashlightHandler;
    public GameController gameController;

    // Start is called before the first frame update
    void Start()
    {
        pickup = false;
        _player = GameObject.FindGameObjectWithTag("Player");

        if (GameMaster.Instance.hasTheFlashlight)
        {
            ChangeTrasformOfThisGameObject();
        }
    }

    private void ChangeTrasformOfThisGameObject()
    {
        if (gameObject.transform.parent != flashlightHandler.transform)
        {
            gameObject.transform.parent = flashlightHandler.transform;
            gameObject.transform.rotation = flashlightHandler.transform.rotation;
            gameObject.transform.localPosition = new Vector3(0, 0, 0);
        }
    }

    void OnTriggerStay(Collider other)
    {
        Debug.Log("COLLIDER: " + other.gameObject.tag);
        if ( (other.gameObject.tag == "Player") && !pickup && !GameMaster.Instance.hasTheFlashlight)
        {
            gameController.FlashlightTexts(true);

            if (gameController.GetFlashlight())
            {
                pickup = true;
                PickUpFlashlight(other.gameObject);
                GameMaster.Instance.hasTheFlashlight = true;
            }
        }
        if (pickup || GameMaster.Instance.hasTheFlashlight)
        {
            gameController.FlashlightTexts(false);
        }
    }

    /*
     * In this method I use a FlashlightHandler (with predefined local position and rotation)
     * which is just an empty game object, child of the MainCamera
     * and with that way I put in the position of camera's view that I want (after pickup of the flashlight)
     */
    private void PickUpFlashlight(GameObject player)
    {
        GameObject handler = GameObject.FindGameObjectWithTag("FlashlightHandler");
        transform.SetParent(handler.transform);
        transform.position = handler.transform.position;
        transform.rotation = handler.transform.rotation;
    }

    private void OnTriggerExit(Collider other)
    {
        if ((other.gameObject.tag == "Player") && !pickup)
        {
            gameController.FlashlightTexts(false);
        }
    }
}
