﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsMenu : MonoBehaviour
{
    public Canvas musicMenuCanvas;
    public Canvas controlMenuCanvas;
    public Canvas gameplayCanvas;

    public Animator cameraAnimator;

    //Store all the secondary canvas (that should be activated or deactivated depending on player's action)
    public GameObject[] allSecondaryCanvas;

    public void OpenSoundSettings()
    {
        controlMenuCanvas.gameObject.SetActive(false);
        gameplayCanvas.gameObject.SetActive(false);

        if (musicMenuCanvas.gameObject.activeInHierarchy)
        {
            musicMenuCanvas.gameObject.SetActive(false);
        }
        else
        {
            musicMenuCanvas.gameObject.SetActive(true);
        }
    }

    public void OpenControlMenu()
    {
        musicMenuCanvas.gameObject.SetActive(false);
        gameplayCanvas.gameObject.SetActive(false);

        if (controlMenuCanvas.gameObject.activeInHierarchy)
        {
            controlMenuCanvas.gameObject.SetActive(false);
        }
        else
        {
            controlMenuCanvas.gameObject.SetActive(true);
        }
    }

    //When the 'Back' button is pressed
    public void TriggerCameraAnimation()
    {
        cameraAnimator.SetTrigger("MoveCamera");
        DisableAllSecondaryCanvas();
    }

    public void DisableAllSecondaryCanvas()
    {
        foreach (GameObject canvas in allSecondaryCanvas)
        {
            canvas.gameObject.SetActive(false);
        }
    }

    public void OpenGameplayMenu()
    {
        controlMenuCanvas.gameObject.SetActive(false);
        musicMenuCanvas.gameObject.SetActive(false);

        if (gameplayCanvas.gameObject.activeInHierarchy)
        {
            gameplayCanvas.gameObject.SetActive(false);
        }
        else
        {
            gameplayCanvas.gameObject.SetActive(true);
        }
    }
}
