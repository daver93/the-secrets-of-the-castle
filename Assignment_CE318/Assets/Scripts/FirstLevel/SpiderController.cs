﻿using UnityEngine;

public class SpiderController : MonoBehaviour
{
    public bool scared;
    public float speed = 2.0f;
    public GameObject targetPosition;

    private void Start()
    {
        scared = false;
    }

    private void Update()
    {
        if (scared)
        {
            float step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, targetPosition.transform.position, step);
            Destroy(this.gameObject, 3);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if ( (other.gameObject.tag == "Player") && !scared)
        {
            scared = true;
        }
    }
}
