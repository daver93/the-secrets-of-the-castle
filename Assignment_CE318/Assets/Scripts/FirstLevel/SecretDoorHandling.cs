﻿using UnityEngine;

public class SecretDoorHandling : MonoBehaviour
{
    public LeverHandler leverHandler;
    public Animator animator;

    private void FixedUpdate()
    {
        if (!leverHandler.isUp)
        {
            animator.SetBool("open", true);
        }
    }
}
