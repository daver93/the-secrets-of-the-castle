﻿using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    public float speed;

    void FixedUpdate()
    {
        float horizontalAxis = Input.GetAxis("Horizontal");
        float verticalAxis = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(horizontalAxis, 0.0f, verticalAxis);

        GetComponent<Rigidbody>().AddForce(movement * speed * Time.deltaTime);
    }
}
