﻿using UnityEngine;
using UnityEngine.AI;

public class Zombie : MonoBehaviour
{
    private GameObject _player;
    private Vector3 _zombiePosition;
    private Vector3 _playerPosition;

    public Animator zombieAnimator;
    public AudioSource zombieAttackSound;
    public bool canSeePlayer;
    public float speed;
    public int layerMask;
    public int nextDestinationPoint = 0;
    public Transform[] patrolPoints;
    public NavMeshAgent zombieAIAgent;

    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        _playerPosition = _player.transform.position;

        _zombiePosition = transform.position;
        zombieAIAgent = GetComponent<NavMeshAgent>();

        // Disabling auto-braking allows for continuous movement between points (ie, the agent doesn't slow down as it approaches a destination point).
        zombieAIAgent.autoBraking = false;

        zombieAnimator.SetBool("run", false);

        // Bit shift the index of the layer (8) to get a bit mask
        layerMask = 1 << 10;

        // This would cast rays only against colliders in layer 8.
        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        layerMask = ~layerMask;
    }

    private void Update()
    {
        PerformRaycasting();
        PlayAudio();
        ChangeAnimationState();
    }

    private void PerformRaycasting()
    {
        RaycastHit hit;
        var enemyRay = new Ray(_zombiePosition, transform.forward);

        if (Physics.Raycast(enemyRay, out hit, Mathf.Infinity, layerMask))
        {
            if (hit.collider.tag == "Player")
            {
                PlayerDetected();
            }
            else
            {
                PlayerUndetected();
            }
        }
    }

    private void PlayerDetected()
    {
        canSeePlayer = true;
        LookAtThePlayer();
        zombieAIAgent.destination = _player.transform.position;
        SeekPlayer();
    }

    public void LookAtThePlayer()
    {
        _playerPosition = _player.transform.position;
        //look at the player (but only in x, z axis)
        transform.LookAt(new Vector3(_playerPosition.x, _zombiePosition.y, _playerPosition.z));
    }

    public void SeekPlayer()
    {
        float step = speed * Time.deltaTime;
        _zombiePosition = Vector3.MoveTowards(_zombiePosition, _playerPosition, step);
    }

    public void PlayAudio()
    {
        if (zombieAnimator.GetBool("run"))
        {
            if (!zombieAttackSound.isPlaying)
            {
                zombieAttackSound.Play();
            }
        }
        else
        {
            if (zombieAttackSound.isPlaying)
            {
                zombieAttackSound.Stop();
            }
        }
    }

    public void ChangeAnimationState()
    {
        if (canSeePlayer && !zombieAnimator.GetBool("run"))
        {
            ZombieAnimatorChangeToRun();
        }
        else if (!canSeePlayer && zombieAnimator.GetBool("run"))
        {
            ZombieAnimatorChangeToWalk();
        }
    }

    private void ZombieAnimatorChangeToRun()
    {
        zombieAnimator.SetBool("run", true);
    }

    private void ZombieAnimatorChangeToWalk()
    {
        zombieAnimator.SetBool("run", false);
    }

    private void PlayerUndetected()
    {
        canSeePlayer = false;
        if (!zombieAIAgent.pathPending && zombieAIAgent.remainingDistance < 0.5f)
        {
            GotoNextPoint();
        }
    }

    public void GotoNextPoint()
    {
        if (patrolPoints.Length == 0)
        {
            return;
        }

        zombieAIAgent.destination = patrolPoints[nextDestinationPoint].position;
        nextDestinationPoint = (nextDestinationPoint + 1) % patrolPoints.Length;
    }
}
