﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public GameObject player;

    public Text doorHandlingText;
    public Text flashlightText;
    public Text leverHandlerText;
    public Text drawerText;
    public Text keyText;
    public Text specialKeyText;

    public GameObject[] doors;

    public LeverHandler leverHandler;

    // Start is called before the first frame update
    void Start()
    {
        doors = GameObject.FindGameObjectsWithTag("Door");
    }

    private void FixedUpdate()
    {
        SetTexts();
    }

    public bool HandleDoor()
    {
        if (Input.GetButtonDown("Open"))
        {
            return true;
        }
        return false;
    }

    public void SetTexts()
    {
        foreach (GameObject door in doors){
            HandleDoor aDoor = door.GetComponent<HandleDoor>();

            if (aDoor.GetLock() && aDoor.playerCollisionWithDoor)
            {
                doorHandlingText.text = "I can't open this door. It's locked!";
                doorHandlingText.gameObject.SetActive(true);
            }
            else
            {
                if (aDoor.isOpen && aDoor.playerCollisionWithDoor)
                {
                    doorHandlingText.text = "Press 'O' to Close the door";
                    doorHandlingText.gameObject.SetActive(true);
                }
                else if (!aDoor.isOpen && aDoor.playerCollisionWithDoor)
                {
                    doorHandlingText.text = "Press 'O' to Open the door";
                    doorHandlingText.gameObject.SetActive(true);
                }
                else if (!aDoor.playerCollisionWithDoor && doorHandlingText)
                {
                    doorHandlingText.gameObject.SetActive(false);
                }
            }
        }
    }

    public void FlashlightTexts(bool show)
    {
        if (show)
        {
            flashlightText.text = "Press 'P' to pickup the flashlight";
            flashlightText.gameObject.SetActive(true);
        }
        else
        {
            flashlightText.gameObject.SetActive(false);
        }
    }

    public bool GetFlashlight()
    {
        if (Input.GetButton("Collect"))
        {
            return true;
        }
        return false;
    }

    public void LeverText(bool show)
    {
        if (show)
        {
            leverHandlerText.text = "Press 'P' to use the lever";
            leverHandlerText.gameObject.SetActive(true);
        }
        else
        {
            leverHandlerText.gameObject.SetActive(false);
        }
    }

    public void DrawerText(bool show)
    {
        if (show)
        {
            drawerText.text = "Press 'O' for the drawer";
            drawerText.gameObject.SetActive(true);
        }
        else
        {
            drawerText.gameObject.SetActive(false);
        }
    }

    public void KeyText(bool show)
    {
        if (show)
        {
            drawerText.text = "Press 'P' to take the key";
            drawerText.gameObject.SetActive(true);
        }
        else
        {
            drawerText.gameObject.SetActive(false);
        }
    }

    public void SpecialKeyText(bool show, bool hasTheKey)
    {
        if (show && hasTheKey)
        {
            specialKeyText.text = "Press 'O' to Open the door";
            specialKeyText.gameObject.SetActive(true);
        }
        else if (show && !hasTheKey)
        {
            specialKeyText.text = "I don't have the key for this door";
            specialKeyText.gameObject.SetActive(true);
        }
        else
        {
            specialKeyText.gameObject.SetActive(false);
        }
    }

}
