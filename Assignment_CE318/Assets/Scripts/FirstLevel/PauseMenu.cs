﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    public GameObject menuCanvas;
    public GameObject mapCamera;

    public LifeHandler lifeHandler;

    public bool showPauseMenu;

    public bool isDead;

    private void Start()
    {
        showPauseMenu = false;
        menuCanvas.gameObject.SetActive(showPauseMenu);

        isDead = false;
    }

    private void Update()
    {

        isDead = lifeHandler.killed;

        if (Input.GetKeyDown(KeyCode.Escape) && !isDead)
        {
            ContinueGame();
        }

        if (!isDead && showPauseMenu)
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = showPauseMenu;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ContinueGame()
    {
        HandleTime();
        showPauseMenu = !showPauseMenu;

        if (showPauseMenu)
        {
            mapCamera.SetActive(false);
        }
        else
        {
            mapCamera.SetActive(true);
        }
        
        menuCanvas.gameObject.SetActive(showPauseMenu);
    }

    public void HandleTime()
    {
        Time.timeScale = Time.timeScale == 0 ? 1 : 0;
    }

    public void MainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

}
