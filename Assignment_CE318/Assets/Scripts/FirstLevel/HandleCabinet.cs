﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandleCabinet : MonoBehaviour
{

    public Animator animator;
    public GameController gameController;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            gameController.DrawerText(true);

            if (Input.GetButtonDown("Open"))
            {
                animator.SetTrigger("CabinetTriggerEvent");
            }   
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            gameController.DrawerText(false);
        }
    }
}
