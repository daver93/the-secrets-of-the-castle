﻿using UnityEngine;

public class UseFlashlight : MonoBehaviour
{
    public Light light;
    private TakeFlashlight _holdFlashlight;

    public bool isDebugging = false;

    private void Start()
    {
        _holdFlashlight = GetComponent<TakeFlashlight>();
    }

    private void FixedUpdate()
    {
        if ( (_holdFlashlight.pickup || GameMaster.Instance.hasTheFlashlight) && Input.GetButtonDown("UseFlashlight"))
        {
            if (light.gameObject.activeInHierarchy)
            {
                light.gameObject.SetActive(false);
            }
            else
            {
                light.gameObject.SetActive(true);
            }
        }
    }
}
