﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChangeController : MonoBehaviour
{
    public Animator animator;

    public string levelName;

    public void FaidLevel(string name)
    {
        animator = GetComponent<Animator>();
        levelName = name;
        animator.SetTrigger("FadeOut");
    }

    public void OnFadeComplete()
    {
        SceneManager.LoadScene(levelName);
    }
}
