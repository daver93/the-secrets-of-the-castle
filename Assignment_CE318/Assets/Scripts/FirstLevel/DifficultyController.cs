﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyController : MonoBehaviour
{

    public GameObject zombiesDifficultMode;

    private void Start()
    {
        if (GameMaster.Instance.difficulty == 1)
        {
            zombiesDifficultMode.SetActive(true);
        }
        else
        {
            zombiesDifficultMode.SetActive(false);
        }
    }


}
