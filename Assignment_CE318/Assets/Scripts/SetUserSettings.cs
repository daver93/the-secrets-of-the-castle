﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetUserSettings : MonoBehaviour
{

    public AudioSource[] audioSources;

    public bool playMusic;
    public float levelOfMusic;

    private void Start()
    {

        playMusic = GameMaster.Instance.playMusic;
        levelOfMusic = GameMaster.Instance.musicLevel;

        audioSources = GetComponents<AudioSource>();

        foreach (AudioSource audioSource in audioSources)
        {
            if (playMusic)
            {
                audioSource.volume = levelOfMusic;
            }
            else
            {
                audioSource.volume = 0;
            }
        }
    }

}
