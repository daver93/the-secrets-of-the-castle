﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keys : MonoBehaviour
{

    public bool hasSilverKey;
    public bool hasFinalKey;

    public GameController gameController;

    private void Start()
    {
        hasSilverKey = false;
        hasFinalKey = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "SilverKey")
        {
            gameController.KeyText(true);
            if (Input.GetButtonDown("Collect"))
            {
                hasSilverKey = true;
                GameMaster.Instance.hasTheSilverKey = true;
                Destroy(other.gameObject);
                gameController.KeyText(false);
            }
        }
        else if (other.gameObject.tag == "GoldKey")
        {
            gameController.KeyText(true);
            if (Input.GetButtonDown("Collect"))
            {
                hasFinalKey = true;
                GameMaster.Instance.hasTheGoldenKey = true;
                Destroy(other.gameObject);
                gameController.KeyText(false);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        gameController.KeyText(false);
    }
}
