﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandleSpecialDoors : MonoBehaviour
{

    public GameObject silverDoor;
    public GameObject goldenDoor;

    public GameController gameController;

    public Animator specialDoorAnimation;

    public bool hasTheSilverKey;
    public bool hasTheGoldenKey;
    public bool isOpen;

    private void Start()
    {
        silverDoor = GameObject.FindGameObjectWithTag("SilverDoor");
        goldenDoor = GameObject.FindGameObjectWithTag("GoldenDoor");
        isOpen = false;
    }

    private void Update()
    {
        if (hasTheSilverKey != GameMaster.Instance.hasTheSilverKey)
        {
            hasTheSilverKey = GameMaster.Instance.hasTheSilverKey;
        }
        if (hasTheGoldenKey != GameMaster.Instance.hasTheGoldenKey)
        {
            hasTheGoldenKey = GameMaster.Instance.hasTheGoldenKey;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" && !isOpen)
        {
            if (this.gameObject.tag == "SilverDoor")
            {
                PlayDoorAnimation(hasTheSilverKey);
            }
            else if (this.gameObject.tag == "GoldenDoor")
            {
                PlayDoorAnimation(hasTheGoldenKey);
            }
        }
    }

    private void PlayDoorAnimation(bool hasTheSpecialKey)
    {
        gameController.SpecialKeyText(true, hasTheSpecialKey);

        if (gameController.HandleDoor() && hasTheSpecialKey)
        {
            specialDoorAnimation.SetBool("isOpen", true);
            isOpen = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            gameController.SpecialKeyText(false, hasTheSilverKey);
        }
    }

}
