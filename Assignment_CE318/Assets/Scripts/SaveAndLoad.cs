﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.SceneManagement;

public class SaveAndLoad : MonoBehaviour
{

    public Transform playerPosition;
    public GameObject flashlight;

    [Serializable]
    class PlayerData
    {
        public bool hasTheSilverKey;
        public bool hasTheGoldenKey;
        public bool hasTheFlashlight;

        public float musicLevel;
        public bool playMusic;
        public int difficulty;

        public bool saved;

        public float playerX;
        public float playerY;
        public float playerZ;
    }

    public void Save()
    {
        string filename = Application.persistentDataPath + "/playInfo.dat";
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(filename, FileMode.OpenOrCreate);

        PlayerData pd = new PlayerData();

        pd.saved = true;

        //Save info about the keys
        pd.hasTheSilverKey = GameMaster.Instance.hasTheSilverKey;
        pd.hasTheGoldenKey = GameMaster.Instance.hasTheGoldenKey;

        pd.hasTheFlashlight = GameMaster.Instance.hasTheFlashlight;

        //Save player's position
        pd.playerX = playerPosition.position.x;
        pd.playerY = playerPosition.position.y;
        pd.playerZ = playerPosition.position.z;

        //Save info about game settings
        pd.difficulty = GameMaster.Instance.difficulty;

        bf.Serialize(file, pd);
        file.Close();
    }

    public void Load()
    {
        string filename = Application.persistentDataPath + "/playInfo.dat";
        if (File.Exists(filename))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(filename, FileMode.Open);

            PlayerData pd = (PlayerData)bf.Deserialize(file);
            file.Close();

            if (pd.saved == true)
            {
                GameMaster.Instance.isSaved = true;

                //Load info about the keys
                GameMaster.Instance.hasTheSilverKey = pd.hasTheSilverKey;
                GameMaster.Instance.hasTheGoldenKey = pd.hasTheGoldenKey;
                GameMaster.Instance.hasTheFlashlight = pd.hasTheFlashlight;

                //Load player's position
                GameMaster.Instance.playerX = pd.playerX;
                GameMaster.Instance.playerY = pd.playerY;
                GameMaster.Instance.playerZ = pd.playerZ;

                //Load info about game settings
                GameMaster.Instance.difficulty = pd.difficulty;

                //Load castle scene
                SceneManager.LoadScene("Scene01");
            }
        }
    }
}
