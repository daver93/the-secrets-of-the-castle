﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public LevelChangeController levelChangeController;
    public Animator cameraAnimator;

    public Slider volumeSlider;
    public Toggle shouldPlayMusic;
    public TMP_Dropdown difficultyDropdownMenu;


    //The following attributes are used in GameMaster
    //These values will be transfered in all scenes
    public float musicLevel;
    public bool playMusic;
    public int difficulty;


    public AudioSource musicAudio;
    
    private void Start()
    {
        difficulty = 0;
        GameMaster.Instance.difficulty = difficulty;

        playMusic = true;
        GameMaster.Instance.playMusic = playMusic;

        musicLevel = 0.4f;
        GameMaster.Instance.musicLevel = musicLevel;
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        levelChangeController.FaidLevel("OutterPlace");
    }

    public void LoadSettingsMenu()
    {
        cameraAnimator.SetTrigger("MoveCamera");
    }

    public void OnVolumeChange()
    {
        musicAudio.volume = volumeSlider.value;
        musicLevel = musicAudio.volume;
        GameMaster.Instance.musicLevel = musicLevel;
    }

    public void PlayMusicOption()
    {
        playMusic = shouldPlayMusic.isOn;
        GameMaster.Instance.playMusic = playMusic;

        if (playMusic)
        {
            musicAudio.Play();
        }
        else
        {
            musicAudio.Stop();
        }
    }

    public void SelectDifficultyLevel()
    {
        difficulty = difficultyDropdownMenu.value;
        Debug.Log("Difficulty: " + difficulty);
        GameMaster.Instance.difficulty = difficulty;
    }
}
