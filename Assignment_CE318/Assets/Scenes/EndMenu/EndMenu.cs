﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class EndMenu : MonoBehaviour
{
    public Animator fadeIn;
    public GameObject fadeImage;

    public bool isWinner;

    public TextMeshProUGUI finalText;

    private void Start()
    {
        isWinner = false;
        isWinner = GameMaster.Instance.isWinner;

        if (isWinner)
        {
            Debug.Log("You Win!");
            finalText.text = "You Win!";
            finalText.gameObject.SetActive(true);
        }
        else
        {
            Debug.Log("GameOver!");
            finalText.text = "GameOver!";
            finalText.gameObject.SetActive(true);
        }

        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void LoadMainMenu()
    {
        fadeImage.SetActive(true);
        fadeIn.SetTrigger("Fade");
    }
}
